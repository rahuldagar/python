from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.views import generic
import time
from .models import Choice, Poll, PollHistory
from django.contrib.auth.decorators import login_required
import json
from django.db import transaction, connection
from django.views.decorators.csrf import csrf_exempt

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Poll.objects.order_by('-pub_date')[:5]



class ResultsView(generic.DetailView):
    model = Poll
    template_name = 'polls/results.html'


# def canVote(poll_id, user_id):
@login_required
def vote(request, poll_id):
    p = get_object_or_404(Poll, pk=poll_id)
    ph = PollHistory.objects.filter(poll_id = p, user_id = request.user.id)
    if len(ph) > 0:
        return HttpResponseRedirect(reverse('polls:results', args=(p.poll_id,)))
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the poll voting form.
        return render(request, 'polls/detail.html', {
            'poll': p,
            'error_message': "You didn't select a choice.",
        })
    else:
        with transaction.atomic():
            selected_choice.votes += 1
            selected_choice.save()
            PollHistory(poll_id = p, user_id = request.user.id).save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(p.poll_id,)))

@login_required
def savePoll(request):
    user_id = request.user.id
    print user_id
    ques_data = json.loads(request.POST['data'])['create']
    ques_title = ques_data['ques']
    ques_opt = ques_data['opt']
    check_len = [True for sin_opt in ques_opt if len(sin_opt) > 300]
    if True in check_len or len(ques_opt) < 2:
        return HttpResponse(json.dumps({"success": False, 'data': "Option Length should be less than 300 and minimum options should be 2"}))
    with transaction.atomic():
        q = Poll(question=ques_title, user_id=user_id)
        q.save()
        for sin_choice in ques_opt:
            c = Choice(poll=q, choice_text=sin_choice)
            c.save()
    return HttpResponse(json.dumps({"success":True,'data':[]}))

@login_required
def createPoll(request):
    return render(request, 'polls/create_poll.html', {})
    pass

@login_required
def votePoll(request, poll_id):
    p = get_object_or_404(Poll, pk=poll_id)
    ch = Choice.objects.filter(poll_id=p)
    ph = PollHistory.objects.filter(poll_id=p, user_id=request.user.id)
    if len(ph) > 0:
        return HttpResponseRedirect(reverse('polls:results', args=(p.poll_id,)))

    # ques = Poll.objects.get(poll_id=p)
    return render(request, 'polls/vote_poll.html', {
        'choices': ch,
        'question': p.question
    })

@login_required
def view_result(request):
    pass
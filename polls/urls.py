from django.conf.urls import patterns, url, include

from . import views


urlpatterns = patterns('',

    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>\d+)/results/$', views.ResultsView.as_view(), name='results'),
    url(r'^(?P<poll_id>\d+)/vote/$', views.vote, name='vote'),
    url(r'createpoll/save/$', views.savePoll, name='vote'),
    url(r'createpoll/$', views.createPoll, name='vote'),
    url(r'votepoll/(?P<poll_id>\d+)$', views.votePoll, name='vote'),
    url(r'votepoll/(?P<poll_id>\d+)/save$', views.vote, name='vote'),
)

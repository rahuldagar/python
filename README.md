Model:-

CREATE TABLE "polls_poll" (

"question" varchar(200) NOT NULL,

"pub_date" datetime NOT NULL,

"user_id" varchar(200) NOT NULL,

"poll_id" integer NOT NULL PRIMARY KEY AUTOINCREMENT

);

CREATE TABLE "polls_choice" (

"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,

"poll_id" integer NOT NULL REFERENCES "polls_poll" ("poll_id"),

"choice_text" varchar(200) NOT NULL,

"votes" integer NOT NULL

);

CREATE TABLE "polls_pollhistory" (

"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,

"user_id" varchar(200) NOT NULL,

"poll_id_id" integer NOT NULL REFERENCES "polls_poll" ("poll_id")

);



All Pools listing:- http://localhost/polls/

Create Poll:- http://localhost/polls/createpoll/

View Results:- http://localhost/polls/4/results/


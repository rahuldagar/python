var createPoll = createPoll || {}

createPoll.init = function(){
        var makeCall = function(data){
            $.ajax({
                type : 'post',
                jsonp:false,
                jsonpCallback : "mycallback",
                dataType: "json",
                url : '//'+window.location.host+'/polls/createpoll/save/',
                data : {'data':data, csrfmiddlewaretoken: $('[name=csrfmiddlewaretoken]').val()},
                headers: {
                    'X-CSRF-Token': $('[name=csrfmiddlewaretoken]').val()
                },
                success :function(data)
                {       if (data.success){
                            console.log(data)
                            console.log("in success")
                            $("#alert_box").text("Poll Saved successfully")
                            window.location.href = "/polls/"
                        }
                        else{

                            $("#alert_box").text("Poll Saved failed " + data.data)
                        }

                },
                error : function(data)
                {
                        $("#alert_box").text("Poll Saving failed")
                }

            });

        }

        var processForm = function(){
             var ques_text = $("#ques_title").val()
             var ques_options = []
             options_boxes = $('[id=ques_options]')
             for(var i=0; i < options_boxes.length; i++){

                    ques_options.push($(options_boxes[i]).val())

             }
             console.log(ques_text)
             console.log(ques_options)
             makeCall(JSON.stringify({create:{ques: ques_text, opt: ques_options}, CSRF: $('[name=csrfmiddlewaretoken]').val()}))
        }

        var addOption = function(){
                total_opt = $('[id=ques_options]').length
                var compiled = _.template('<%= total_opt +1 %>. <input id="ques_options"><br><br>');
                html_text = compiled({'total_opt': total_opt});
                $('#createpoll').append(html_text)
        }

        return {
            'processForm': processForm,
            'addOption': addOption
        }
    }

createPoll.init = createPoll.init()